package pl.decerto.rekrutacja.bpawlowski;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
class HelloResourceV1 {

    @GetMapping("/{name}")
    ValueWrapper<String> hello(@PathVariable("name") String name) {
        return greet(name);
    }

    @GetMapping("/secured")
    ValueWrapper<String> helloSecured(@AuthenticationPrincipal UserDetails userDetails) {
        return greet(userDetails.getUsername());
    }

    private ValueWrapper<String> greet(String name) {
        return new ValueWrapper<>("Hello, %s".formatted(name));
    }
}
